-
Thanks to : rwxrob aka uncleRob see on twitch: https://twitch.tv/rwrxrob
Date		: 13-Mar-2020
-

# Modular basrhc by File Sort Order
This directory contains all the parts of the core Easy Bash Terminal
Configuration as well as extras that acn be disabled or removed entirely. This
allows people pilfering to do so more easily (as in, just a 'wget or curl'
command). The cost on login startup is minimal to put these into their own
files. Yet this is far less complicated than some of the other options out
there.

It also makes it far easier to maintain because you can simple source the
individual files and test their content rather than building the whole thing and
potentially messing up your currently running shell.

## File Ordering

Simple file ordering such as that used in `etc/rc.d` is used with the added
convention of naming everything initially with three digits. Consider the
following guide for numbering:

* **000-099** - Reserved for testing mock ups and things that come *before* the  main configuration framework.

* **100-199** - Reserved for main configuration stuff upon which other extension depend.

* **200-299** - Extension designed to be used by other extensions.

* **500-899** - Extension

* **800-899** - Reserved for main configuration stuff.

* **900-999** - Reserved for testing mock ups and things that always come *after* the main configuration framework.


## Adding Your Own Extensions

To code an extension that works with this system, just create a file with
a `.bash` suffix and store it in one place directly into the `.bashrc.d`
directory or symlink in into it.


## How-to build this extension

After you wrote all `.bash` just run `build*` file.

## License

Public domain; see the included **UNLICENSE** file. It's just configuration and
simple scripts, so do whatever you like with it if any of it's useful to you. If
you're feeling generous please send me an email or push your change into gitlab.

