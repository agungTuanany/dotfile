# shell must be interactive
case $- in
	*i*) ;;
	*) return
esac

# reset all aliases
unalias -a
